//
//  ViewController.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/12/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase
import WatchConnectivity
import WatchKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, WCSessionDelegate {
    
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var currentListButton: UIButton!
    @IBOutlet weak var currentListBar: UIView!
    @IBOutlet weak var recentlyDeletedButton: UIButton!
    @IBOutlet weak var recentlyDeletedBar: UIView!
    @IBOutlet weak var peopleWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var peopleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var peopleMenuButton: UIButton!
    @IBOutlet weak var leaveMenuButton: UIButton!
    @IBOutlet weak var settingsMenuButton: UIButton!
    
    var leaveButtonCenter: CGPoint!
    var settingsButtonCenter: CGPoint!
    var menuBaseButtonHeight: CGFloat!
    var menuBaseButtonWidth: CGFloat!
    var signedUser: SignedInUser = SignedInUser(_groceryList: [], _name: "", _email: "", _listID: "")
    var deletedGroceries = [String]()
    let selectedColor = UIColor(red:0.12, green:0.42, blue:0.19, alpha:1.0)
    var removedCellIndex: IndexPath!
    var session: WCSession!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observers()
        if (WCSession.isSupported()) {
            session = WCSession.default
            session.delegate = self
            session.activate()
        }
        currentListPress(currentListButton)
        menuBaseButtonWidth = peopleWidthConstraint.constant
        menuBaseButtonHeight = peopleHeightConstraint.constant
        leaveButtonCenter = leaveMenuButton.center
        settingsButtonCenter = settingsMenuButton.center
        setMenuButtonSize(btn: peopleMenuButton)
        setMenuButtonSize(btn: leaveMenuButton)
        setMenuButtonSize(btn: settingsMenuButton)
        hideMenuButtons()
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /////////SESSION\\\\\\\\
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?){
        DispatchQueue.main.async {
            let data = NSKeyedArchiver.archivedData(withRootObject: self.signedUser)
            session.sendMessage(["setGrocery": data], replyHandler: { (reply) in
                print(reply)
            }, errorHandler: { (error) in
                print(error)
            })
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        DispatchQueue.main.async {
            if (message["GetGroceryList"] as? Bool) != nil{
                let ref = Database.database().reference()
                if let cUser = FirebaseHelper.shared.currentUser{
                    ref.child("users").child(cUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                        let value = snapshot.value as? NSDictionary
                        let name = value?["Name"] as? String ?? "ERROR"
                        let listID = value?["Shared List ID"] as? String ?? "ERROR"
                        ref.child("lists").child(listID).observe(.value, with: { snapshot in
                            var groceryList: [GroceryItem] = []
                            let value = snapshot.value as! NSDictionary
                            if let itemsArray = value["Items"] as? [[String: Any]]{
                                for itemRef in itemsArray{
                                    groceryList.append(GroceryItem.init(_itemName: itemRef["name"] as! String, _createdBy: itemRef["createdBy"] as! String))
                                }
                            }
                            let userObject = SignedInUser(_groceryList: groceryList, _name: name, _email: cUser.email!, _listID: listID)
                            NSKeyedArchiver.setClassName("SignedInUser", for: SignedInUser.self)
                            let data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                            replyHandler(["NewItem": data])
                        })
                    })
                }
            } else if let newList = message["updateGrocery"] as? Data{
                NSKeyedUnarchiver.setClass(SignedInUser.self, forClassName: "SignedInUser")
                if let object = NSKeyedUnarchiver.unarchiveObject(with: newList) as? SignedInUser{
                    self.signedUser = object
                    self.updateFirebaseList()
                    self.listTableView.reloadData()
                }
            }
        }
    }
    
    ////////MENU\\\\\\\\\
    
    @IBAction func addButtonPressed(_ sender: Any) {
        //removeUndoCell()
        let alert = UIAlertController(title: "New Item", message: "What would you like to add to the list?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Enter Item Name"
        }
        alert.addAction(UIAlertAction(title: "Add", style: .default) { (_) in
            //getting the input values from user
            if let itemName = alert.textFields?[0].text{
                if !itemName.isEmpty{
                    self.signedUser.groceryList.append(GroceryItem(_itemName: itemName, _createdBy: self.signedUser.name))
                    self.listTableView.reloadData()
                    self.updateFirebaseList()
                    
                }
            }
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func meunButtonPressed(_ sender: Any) {
        if menuButton.image == #imageLiteral(resourceName: "menu"){
            menuButton.image = #imageLiteral(resourceName: "error (1)")
            showMenuButtons()
        }else{
            hideMenuButtons()
        }
    }
    
    @IBAction func peopleBtnPress(_ sender: Any) {
        hideMenuButtons()
        performSegue(withIdentifier: "toPeopleView", sender: nil)
    }
    
    @IBAction func signOutButtonPress(_ sender: Any) {
        do{
            try Auth.auth().signOut()
        }catch{
            let alert = UIAlertController(title: "Error",
                                          message: "There was an error signing you out... try again later",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func settingsBtnPress(_ sender: Any) {
        hideMenuButtons()
        performSegue(withIdentifier: "toSettings", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PeopleViewController{
            destination.listId = signedUser.listID
        }
    }
    
    func hideMenuButtons(){
        menuButton.image = #imageLiteral(resourceName: "menu")
        leaveMenuButton.center = peopleMenuButton.center
        settingsMenuButton.center = peopleMenuButton.center
        peopleWidthConstraint.constant = 0
        peopleHeightConstraint.constant = 0
        peopleMenuButton.layoutIfNeeded()
        peopleMenuButton.isHidden = true
        leaveMenuButton.isHidden = true
        settingsMenuButton.isHidden = true
    }
    
    func showMenuButtons(){
        UIView.animate(withDuration: 0.3) {
            self.peopleMenuButton.isHidden = false
            self.leaveMenuButton.isHidden = false
            self.settingsMenuButton.isHidden = false
            self.peopleWidthConstraint.constant = self.menuBaseButtonWidth
            self.peopleHeightConstraint.constant = self.menuBaseButtonHeight
            self.peopleMenuButton.layoutIfNeeded()
            self.leaveMenuButton.center = self.leaveButtonCenter
            self.settingsMenuButton.center = self.settingsButtonCenter
        }
    }
    
    func setMenuButtonSize(btn: UIButton){
        btn.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        btn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btn.layer.cornerRadius = 0.5 * btn.bounds.size.width
    }
    
    ////////TOOL BAR\\\\\\\\\
    
    @IBAction func recentlyDeletedPress(_ sender: Any) {
        //removeUndoCell()
        currentListBar.backgroundColor = UIColor.white
        currentListButton.setTitleColor(UIColor.white, for: .normal)
        recentlyDeletedBar.backgroundColor = selectedColor
        recentlyDeletedButton.setTitleColor(selectedColor, for: .normal)
        listTableView.reloadData()
    }
    
    
    @IBAction func currentListPress(_ sender: Any) {
        //removeUndoCell()
        recentlyDeletedBar.backgroundColor = UIColor.white
        recentlyDeletedButton.setTitleColor(UIColor.white, for: .normal)
        currentListBar.backgroundColor = selectedColor
        currentListButton.setTitleColor(selectedColor, for: .normal)
        listTableView.reloadData()
    }
    
    //////TABLE VIEW\\\\\\\
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentListBar.backgroundColor == selectedColor{
            return signedUser.groceryList.count
        }else{
            return deletedGroceries.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuse_id_1")!
        cell.backgroundColor = UIColor.white
        if currentListBar.backgroundColor == selectedColor{
            cell.textLabel?.text = signedUser.groceryList[indexPath.row].itemName
        }else{
            cell.textLabel?.text = deletedGroceries[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            //if currentListBar.backgroundColor == selectedColor{
//                deleteItem(index: indexPath.row)
                let deletedItem = signedUser.groceryList.remove(at: indexPath.row)
                deletedGroceries.append(deletedItem.itemName)
            updateFirebaseList()
//            }else{
//                deletedGroceries.remove(at: indexPath.row)
//            }
            tableView.reloadData()
        }
    }
    
    //    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    //        removeUndoCell()
    //        let customDelete = UITableViewRowAction(style: .default, title: "Remove", handler: { (action, index) in
    //            tableView.reloadRows(at: [indexPath], with: .automatic)
    //            let cell = tableView.cellForRow(at: indexPath)!
    //            cell.backgroundColor = UIColor(red:0.70, green:1.00, blue:0.35, alpha:1.0)
    //            cell.textLabel?.text = "Item Removed"
    //            self.removedCellIndex = indexPath
    //        })
    //        return [customDelete]
    //    }
    
    //    func removeUndoCell(){
    //        if removedCellIndex != nil{
    //            deletedGroceries.append(listOfGroceries.remove(at: removedCellIndex.row))
    //            removedCellIndex = nil
    //            listTableView.reloadData()
    //        }
    //    }
    
    //////Fire Base\\\\\\\
    
    func updateFirebaseList(){
        let listRef = Database.database().reference(withPath: "lists").child(signedUser.listID)
        listRef.updateChildValues(signedUser.listToAnyObject(currentUserID: FirebaseHelper.shared.getUserId()))
    }
    
//    func deleteItem(index:Int){
//        let listRef = Database.database().reference(withPath: "lists").child(signedUser.listID).child("Items").child("\(index)")
//        listRef.removeValue()
//    }
    
    func observers(){
        if FirebaseHelper.shared.currentUser == nil {
            return
        }
        let ref = Database.database().reference()
        let listRef = ref.child("lists").child(signedUser.listID).child("Items")
        listRef.observe(.value, with: { snapshot in
            var groceryList: [GroceryItem] = []
            if let itemsArray = snapshot.value as? [[String: Any]]{
                for itemRef in itemsArray{
                    groceryList.append(GroceryItem.init(_itemName: itemRef["name"] as! String, _createdBy: itemRef["createdBy"] as! String))
                }
            }
            self.signedUser.groceryList = groceryList
            self.listTableView.reloadData()
            let data = NSKeyedArchiver.archivedData(withRootObject: self.signedUser)
            self.session.sendMessage(["setGrocery": data], replyHandler: { (reply) in
                print(reply)
            }, errorHandler: { (error) in
                print(error)
            })
        })
    }
}


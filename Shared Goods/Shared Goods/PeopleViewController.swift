//
//  PeopleViewController.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/22/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import FirebaseDatabase

class PeopleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewOut: UITableView!
    @IBOutlet weak var testLabel: UILabel!
    var listId: String!
    var nameList: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        testLabel.text = listId
        let ref = Database.database().reference().child("lists").child(listId).child("User IDs");
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            
            let value = snapshot.value as! [[String: Any]]
            for id in value{
                self.nameList.append(id["name"] as! String)
            }
            self.tableViewOut.reloadData()
        }){ (error) in
            print(error.localizedDescription)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPress(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addPeopleBtnPress(_ sender: Any) {
        if listId != nil{
            let shareActvity = UIActivityViewController(activityItems: ["Enter this list id \"\(listId!)\" to join my grocery list on the app Shared Goods!"], applicationActivities: nil)
            self.present(shareActvity, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "poepleCell", for: indexPath)
        cell.textLabel?.text = nameList[indexPath.row]
        return cell
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

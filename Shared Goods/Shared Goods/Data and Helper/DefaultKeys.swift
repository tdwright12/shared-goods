//
//  DefaultKeys.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/17/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import Foundation

class DefaultKeys{
    static let USER_ID_KEY = "USER_ID_KEY"
    static let USER_EMAIL_KEY = "USER_EMAIL_KEY"
    
    private init(){}
    
    static func setUserDefault(){
        let cUser = FirebaseHelper.shared.currentUser
        UserDefaults.standard.set(cUser?.uid, forKey: USER_ID_KEY)
        UserDefaults.standard.set(cUser?.email, forKey: USER_EMAIL_KEY)
    }
}

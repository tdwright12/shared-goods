//
//  FirebaseHelper.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/16/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//
import Foundation
import Firebase

class FirebaseHelper{
    public var currentUser: User!
    private var listID: String!
    static let shared = FirebaseHelper()
    
    private init() {
        if let currentUser = Auth.auth().currentUser{
            self.currentUser = currentUser
        }else{
            currentUser = nil
        }
    }
    
    func checkForSignIn() -> Bool{
        return currentUser != nil
    }
    
    func getUserData() -> SignedInUser!{
        var userData: SignedInUser!
        if (currentUser != nil){
            let ref = Database.database().reference()
            ref.child("users").child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as? NSDictionary
                let name = value?["Name"] as? String ?? "ERROR"
                self.listID = value?["SharedListID"] as? String ?? "ERROR"
                ref.child("lists").child(self.listID).observeSingleEvent(of: .value, with:  { (snapshot) in
                    var groceryList: [GroceryItem] = []
                    let value = snapshot.value as! NSDictionary
                    let itemsArray = value["Items"] as! [[String: Any]]
                    for itemRef in itemsArray{
                        groceryList.append(GroceryItem.init(_itemName: itemRef["name"] as! String, _createdBy: itemRef["createdBy"] as! String))
                    }
                    userData = SignedInUser(_groceryList: groceryList, _name: name, _email: "self.currentUser.email!", _listID: self.listID)
                })
            }) { (error) in
                print(error.localizedDescription)
            }
        }
        return userData
    }
    
    func signUp(_email: String, _password:String, _name: String){
        var userS: SignedInUser!
        
        Auth.auth().createUser(withEmail: _email, password: _password) { (user, error) in
            if error == nil {
                Auth.auth().signIn(withEmail: _email,
                                   password: _password)
                //Auth.auth().currentUser?.sendEmailVerification {(error) in}
                if user != nil{
                    self.currentUser = user
                    let listRef = Database.database().reference(withPath: "lists")
                    let newListRef = listRef.childByAutoId()
                    userS = SignedInUser(_groceryList: [], _name: _name, _email: _email, _listID: newListRef.key)
                    //newListRef.updateChildValues(userS.listToAnyObject())
                    let usersRef = Database.database().reference(withPath: "users")
                    usersRef.updateChildValues([self.currentUser.uid: userS.userToAnyObject()])
                }
            }
        }
    }
    
    func signIn(_email: String, _password: String){
        currentUser = Auth.auth().currentUser
        print(currentUser.uid)
    }
    
    public func getUserId() -> String{
        return currentUser.uid
    }
}

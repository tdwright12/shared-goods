//
//  GroceryItem.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/13/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import Foundation
@objc(GroceryItem)
class GroceryItem: NSObject, NSCoding{
    var itemName: String
    var createdBy: String
    
    init(_itemName: String, _createdBy: String!) {
        itemName = _itemName
        if (_createdBy == nil){
            createdBy = "Not set"
        }else{
            createdBy = _createdBy
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        let name = aDecoder.decodeObject(forKey: "itemName") as! String
        let createdBy = aDecoder.decodeObject(forKey: "createdBy") as! String
        self.init(_itemName: name, _createdBy: createdBy)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(itemName, forKey: "itemName")
        aCoder.encode(createdBy, forKey: "createdBy")
    }
}

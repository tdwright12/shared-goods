//
//  User.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/16/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import Foundation
@objc(SignedInUser)
class SignedInUser: NSObject, NSCoding {
    var groceryList: [GroceryItem]
    var name: String
    var email: String
    var listID: String
    
    init(_groceryList: [GroceryItem], _name: String, _email:String, _listID: String) {
        groceryList = _groceryList
        name = _name
        email = _email
        listID = _listID
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        let list = aDecoder.decodeObject(forKey: "groceryList") as! [GroceryItem]
        let name = aDecoder.decodeObject(forKey: "userName") as! String
        let email = aDecoder.decodeObject(forKey: "email") as! String
        let listId = aDecoder.decodeObject(forKey: "listID") as! String
        self.init(_groceryList: list, _name: name, _email: email, _listID: listId)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(groceryList, forKey: "groceryList")
        aCoder.encode(name, forKey: "userName")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(listID, forKey: "listID")
    }
    
    func listToAnyObject(currentUserID: String) -> [AnyHashable: Any] {
        var groceryListData: [[String: AnyObject]] = []
        var usersArray: [[String: AnyObject]] = []
        if groceryList.count > 0{
            for item in groceryList{
                var itemDict: [String: AnyObject] = [:]
                itemDict.updateValue(item.itemName as AnyObject, forKey: "name")
                itemDict.updateValue(item.createdBy as AnyObject, forKey: "createdBy")
                groceryListData.append(itemDict)
            }
            var userDict: [String: AnyObject] = [:]
            userDict.updateValue(name as AnyObject, forKey: "name")
            userDict.updateValue(currentUserID as AnyObject, forKey: "uuid")
            usersArray.append(userDict)
        }
        return [
            "Items": groceryListData,
            "User IDs": usersArray
        ]
    }
    
    func userToAnyObject() -> Any {
        return [
            "Email": email,
            "Shared List ID": listID,
            "Name": name
        ]
    }
}

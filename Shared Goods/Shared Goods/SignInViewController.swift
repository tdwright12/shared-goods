//
//  SignInViewController.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/16/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {
    
    var signedUser: SignedInUser!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func signInBtnPress(_ sender: Any) {
        let email = emailTxt.text!
        let password = passwordTxt.text!
        if email.isEmpty && password.isEmpty{
            let alert = UIAlertController(title: "Error",
                                          message: "Please do not leave any fields blank.",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }else{
            Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                if error == nil{
                    if let currentUser = Auth.auth().currentUser{
                        FirebaseHelper.shared.currentUser = currentUser
                    }
                    DefaultKeys.setUserDefault()
                    self.getData()
                }else{
                    let splitError = error.debugDescription.split(separator: "\"", maxSplits: 3, omittingEmptySubsequences: true)
                    let s = splitError[2].folding(locale: Locale.current)
                    let alert = UIAlertController(title: "Error",
                                                  message: s,
                                                  preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok",
                                                 style: .default)
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
    
    @IBAction func backButtonPress(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func getData() {
        let ref = Database.database().reference()
        ref.child("users").child(FirebaseHelper.shared.currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let name = value?["Name"] as? String ?? "ERROR"
            let listID = value?["Shared List ID"] as? String ?? "ERROR"
            ref.child("lists").child(listID).observeSingleEvent(of: .value, with:  { (snapshot) in
                var groceryList: [GroceryItem] = []
                let value = snapshot.value as! NSDictionary
                if let itemsArray = value["Items"] as? [[String: Any]]{
                    for itemRef in itemsArray{
                        groceryList.append(GroceryItem.init(_itemName: itemRef["name"] as! String, _createdBy: itemRef["createdBy"] as! String))
                    }
                }
                self.signedUser = SignedInUser(_groceryList: groceryList, _name: name, _email: FirebaseHelper.shared.currentUser.email!, _listID: listID)
                self.performSegue(withIdentifier: "signInComplete", sender: nil)
            })
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navController = segue.destination as? UINavigationController{
            if let destinationVC = navController.viewControllers.first as? ViewController{
                destinationVC.signedUser = signedUser
            }
        }
    }
}

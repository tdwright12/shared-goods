//
//  ListVerifyViewController.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/22/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class ListVerifyViewController: UIViewController {
    
    var loginInformation: [String]! //[email,password,name]
    @IBOutlet weak var greetingsLbl: UILabel!
    @IBOutlet weak var listCodeTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        greetingsLbl.text = "Hi \(loginInformation[2]), please enter in the list code for the list that you want to connect to."
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func continueBtnPress(_ sender: Any) {
        let listCode = listCodeTxt.text!
        if !listCode.isEmpty{
            let ref = Database.database().reference()
            ref.child("lists").observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as! NSDictionary
                if value[listCode] == nil{
                    let alert = UIAlertController(title: "Error",
                                                  message: "No list were found with the code entered.",
                                                  preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok",
                                                 style: .default)
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }else{
                    let unparsedObject = value.value(forKey: listCode) as! [String: Any]
                    var list: [GroceryItem] = []
                    if let unparsedList = unparsedObject["Items"] as? [[String: Any]] {
                        for item in unparsedList{
                            list.append(GroceryItem(_itemName: item["name"] as! String, _createdBy: item["createdBy"] as! String))
                        }
                    }
                    self.signUp(groceryList: list)
                }
            }){ (error) in
                print(error.localizedDescription)
            }
        }else{
            let alert = UIAlertController(title: "Error",
                                          message: "Please do not leave any fields blank.",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func signUp(groceryList: [GroceryItem]){
        var userS: SignedInUser!
        let email = loginInformation[0]
        let password = loginInformation[1]
        let name = loginInformation[2]
        let listCode = listCodeTxt.text!
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if error == nil {
                if user != nil{
                    Auth.auth().signIn(withEmail: email,
                                       password: password, completion: { (user, error) in
                                        if error == nil{
                                            if let currentUser = user{
                                                FirebaseHelper.shared.currentUser = currentUser
                                            }
                                            DefaultKeys.setUserDefault()
                                            let listRef = Database.database().reference(withPath: "lists").child(listCode)
                                            userS = SignedInUser(_groceryList: groceryList, _name: name, _email: email, _listID: listCode)
                                            listRef.child("User IDs").updateChildValues(["": user!.uid]);////////////////FIX
                                            let usersRef = Database.database().reference(withPath: "users")
                                            usersRef.updateChildValues([(user?.uid)!: userS.userToAnyObject()])
                                            DefaultKeys.setUserDefault()
                                            DispatchQueue.main.async {
                                                self.performSegue(withIdentifier: "listVerifyComplete", sender: userS)
                                            }
                                        }else{
                                            let splitError = error.debugDescription.split(separator: "\"", maxSplits: 3, omittingEmptySubsequences: true)
                                            let s = splitError[2].folding(locale: Locale.current)
                                            let alert = UIAlertController(title: "Error",
                                                                          message: s,
                                                                          preferredStyle: .alert)
                                            let okAction = UIAlertAction(title: "Ok",
                                                                         style: .default)
                                            alert.addAction(okAction)
                                            self.present(alert, animated: true, completion: nil)
                                        }
                    })
                }
            }
        }
    }
    
    @IBAction func backButtonPress(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selecetedNC = segue.destination as? UINavigationController{
            if let destinationVC = selecetedNC.viewControllers.first as? ViewController{
                destinationVC.signedUser = sender as! SignedInUser
            }
        }
    }
}

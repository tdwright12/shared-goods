//
//  SignUpViewController.swift
//  Shared Goods
//
//  Created by Trae Wright on 3/16/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import BWWalkthrough
import Firebase


class SignUpViewController: UIViewController, BWWalkthroughViewControllerDelegate {
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var rePasswordTxt: UITextField!
    @IBOutlet weak var newListSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let currentUser = Auth.auth().currentUser{
            getData(currentUser: currentUser)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let userDefaults = UserDefaults.standard
        if !userDefaults.bool(forKey: "walkthroughPresented") && !FirebaseHelper.shared.checkForSignIn() {
            showWalkthrough()
            userDefaults.set(true, forKey: "walkthroughPresented")
            userDefaults.synchronize()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //////////WALKTHROUGH\\\\\\\\\
    
    @IBAction func showWalkthrough(){
        // Get view controllers and build the walkthrough
        let walkthroughStoryBoard = UIStoryboard(name: "Walkthrough", bundle: nil)
        let walkthrough = walkthroughStoryBoard.instantiateViewController(withIdentifier: "Base") as! BWWalkthroughViewController
        let page_zero = walkthroughStoryBoard.instantiateViewController(withIdentifier: "Page 1")
        let page_one = walkthroughStoryBoard.instantiateViewController(withIdentifier: "Page 2")
        let page_two = walkthroughStoryBoard.instantiateViewController(withIdentifier: "Page 3")
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.add(viewController: page_zero)
        walkthrough.add(viewController: page_one)
        walkthrough.add(viewController: page_two)
        self.present(walkthrough, animated: true, completion: nil)
    }
    
    
    @IBAction func signUpBtnPress(_ sender: Any) {
        let email = emailTxt.text!
        let password = passwordTxt.text!
        let name = nameTxt.text!
        if password == rePasswordTxt.text && passwordTxt.text != ""{
            if (newListSwitch.isOn){
                signUp(_email: email, _password: password, _name: name)
            }else{
                let loginInformation: [String] = [email,password,name]
                performSegue(withIdentifier: "toListVerify", sender: loginInformation)
            }
        }else{
            let alert = UIAlertController(title: "Error",
                                          message: "Your passwords do not match or they are blank.",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func signUp(_email: String, _password:String, _name: String){
        var userS: SignedInUser!
        Auth.auth().createUser(withEmail: _email, password: _password) { (user, error) in
            if error == nil {
                //Auth.auth().currentUser?.sendEmailVerification {(error) in}
                if user != nil{
                    Auth.auth().signIn(withEmail: _email,
                                       password: _password, completion: { (user, error) in
                                        if error == nil{
                                            if let currentUser = Auth.auth().currentUser{
                                                FirebaseHelper.shared.currentUser = currentUser
                                            }

                                            DefaultKeys.setUserDefault()
                                        }else{
                                            let splitError = error.debugDescription.split(separator: "\"", maxSplits: 3, omittingEmptySubsequences: true)
                                            let s = splitError[2].folding(locale: Locale.current)
                                            let alert = UIAlertController(title: "Error",
                                                                          message: s,
                                                                          preferredStyle: .alert)
                                            let okAction = UIAlertAction(title: "Ok",
                                                                         style: .default)
                                            alert.addAction(okAction)
                                            self.present(alert, animated: true, completion: nil)
                                        }
                    })
                    let listRef = Database.database().reference(withPath: "lists")
                    let newListRef = listRef.childByAutoId()
                    userS = SignedInUser(_groceryList: [GroceryItem.init(_itemName: "Your first item", _createdBy: "Shared Goods")], _name: _name, _email: _email, _listID: newListRef.key)
                    newListRef.updateChildValues(userS.listToAnyObject(currentUserID: (user?.uid)!))
                    let usersRef = Database.database().reference(withPath: "users")
                    usersRef.updateChildValues([(user?.uid)!: userS.userToAnyObject()])
                    DefaultKeys.setUserDefault()
                    Messaging.messaging().subscribe(toTopic: "/topics/newItem")
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "FinishSignUp", sender: userS)
                    }
                }
            }
        }
    }
    
    func getData(currentUser:User){
        let ref = Database.database().reference()
        var userS: SignedInUser!
        ref.child("users").child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let name = value?["Name"] as? String ?? "ERROR"
            let listID = value?["Shared List ID"] as? String ?? "ERROR"
            ref.child("lists").child(listID).observeSingleEvent(of: .value, with:  { (snapshot) in
                var groceryList: [GroceryItem] = []
                let value = snapshot.value as! NSDictionary
                if let itemsArray = value["Items"] as? [[String: Any]]{
                    for itemRef in itemsArray{
                        groceryList.append(GroceryItem.init(_itemName: itemRef["name"] as! String, _createdBy: itemRef["createdBy"] as! String))
                    }
                }
                userS = SignedInUser(_groceryList: groceryList, _name: name, _email: currentUser.email!, _listID: listID)
                DispatchQueue.main.async {//Check this feature.. Temporary fix
                    self.performSegue(withIdentifier: "FinishSignUp", sender: userS)
                }
                
            })
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    @IBAction func unwindToSignIn(segue:UIStoryboardSegue) { }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selecetedNC = segue.destination as? UINavigationController{
            if let destinationVC = selecetedNC.viewControllers.first as? ViewController{
                destinationVC.signedUser = sender as! SignedInUser
            }
        }else if let destinationVC = segue.destination as? ListVerifyViewController{
            destinationVC.loginInformation = sender as! [String]
        }
    }
}

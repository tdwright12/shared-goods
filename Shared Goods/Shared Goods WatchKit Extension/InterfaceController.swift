//
//  InterfaceController.swift
//  Shared Goods WatchKit Extension
//
//  Created by Trae Wright on 3/12/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet var tableViewOutlet: WKInterfaceTable!
    var signedUser: SignedInUser!
    var selectedRow: Int!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?){
        //        if session.isReachable{
        //            let myValues:[String: Any] = ["GetGroceryList":true]
        //            session.sendMessage(myValues, replyHandler: {
        //                (replyData) -> Void in
        //                if replyData["NewItem"] != nil{
        //                    let data = replyData["NewItem"]
        //                    NSKeyedUnarchiver.setClass(SignedInUser.self, forClassName: "SignedInUser")
        //                    if let userObject = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? SignedInUser{
        //                        self.signedUser = userObject
        //                        self.updateTable()
        //                    }
        //                }
        //            }, errorHandler: nil)
        //        }//session exists and reachable
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        DispatchQueue.main.async {
            let newList = message["setGrocery"] as! Data
           NSKeyedUnarchiver.setClass(SignedInUser.self, forClassName: "SignedInUser")
            if let object = NSKeyedUnarchiver.unarchiveObject(with: newList) as? SignedInUser{
                self.signedUser = object
                self.updateTable()
            }
        }
    }
    
    //Just some basic labels for us to display data to on our interface once we get it back from the iPhone side.
    @IBOutlet weak var balanceText: WKInterfaceLabel!
    @IBOutlet weak var dateText: WKInterfaceLabel!
    var session : WCSession!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported()) {
            session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        selectedRow = rowIndex
        pushController(withName: "DeleteAction", context: self)
    }
    
    func updateTable() {
        tableViewOutlet.setNumberOfRows(signedUser.groceryList.count, withRowType: "watchReuseId")
        for(index, item) in self.signedUser.groceryList.enumerated(){
            let row = tableViewOutlet.rowController(at: index) as! GroceryTableCell
            row.itemNameLbl.setText(item.itemName)
        }
    }
    
    func getSelectedItem()->GroceryItem{
        return signedUser.groceryList[selectedRow]
    }
    
    func didDeleteItem(){
        signedUser.groceryList.remove(at: selectedRow)
        let data = NSKeyedArchiver.archivedData(withRootObject: self.signedUser)
        self.session.sendMessage(["updateGrocery": data], replyHandler: { (reply) in
            print(reply)
        }, errorHandler: { (error) in
            print(error)
        })
        updateTable()
    }
}

class GroceryTableCell : NSObject{
    @IBOutlet var itemNameLbl: WKInterfaceLabel!
    
}

class WatchDeleteView: WKInterfaceController {
    var item: GroceryItem!
    var delegate: InterfaceController?
    @IBOutlet var itemNameLbl: WKInterfaceLabel!
    @IBOutlet var createdByLbl: WKInterfaceLabel!
    
    //Recives the context passed from InterfaceController Class
    override func awake(withContext context: Any?) {
        delegate = context as? InterfaceController
        item = delegate?.getSelectedItem()
    }
    
    //Sets the detials view with the recieved data
    override func willActivate() {
        super.willActivate()
        itemNameLbl.setText(item.itemName)
        createdByLbl.setText("Created by: "+item.createdBy)
    }
    
    @IBAction func deletePress() {
        delegate?.didDeleteItem()
        popToRootController()
    }
    
    
    @IBAction func cancelPress() {
        popToRootController()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//    response.send("Hello from Firebase!");
// });


      exports.sendNotification = functions.database.ref('/lists/{listId}/Items')
          .onWrite(event => {
              const payload = {
                 notification: {
                   title: "New Item",
                   body: "A new item has been added to the list."
                 }
               };

               const options = {
                 priority: "high"
               };
                  return admin.messaging().sendToTopic("/topics/newItem", payload, options)
              });
